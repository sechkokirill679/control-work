#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

int nextSmallerThan(int);
void swap(int&, int&);
void reverseSort(int*, int,int);
void nextSmallerThan(string, string);
void nextSmallerThanTests();
bool nextSmallerThanTests(string, string);

int main()
{
	nextSmallerThanTests();
	nextSmallerThan("input.txt", "result.txt");
	nextSmallerThanTests("input.txt", "result.txt");
	system("pause");
}

int nextSmallerThan(int number)
{
    int temp = number;
    int length = 0;

    while (temp > 0) {
        temp /= 10;
        length++;
    }

    if (length <= 1) {
        return -1;
    }

    int* digits = new int[length];

    temp = number;
    for (int i = length - 1; i >= 0; --i) {
        digits[i] = temp % 10;
        temp /= 10;
    }

    int i = length - 2;
    while (i >= 0 && digits[i] <= digits[i + 1]) {
        --i;
    }

    if (i < 0) {
        delete[] digits;
        return -1;
    }

    int j = length - 1;
    while (digits[j] >= digits[i]) {
        --j;
    }

    swap(digits[i], digits[j]);

    reverseSort(digits, i + 1, length);

    int result = 0;
    bool isZero = true;
    for (int k = 0; k < length; ++k) {
        if (isZero && digits[k] == 0) {
            delete[] digits;
            return -1;
        }
        isZero = false;
        result = result * 10 + digits[k];
    }

    delete[] digits;

    if (result >= number) {
        return -1;
    }

    return result;
}

void swap(int& a, int& b) {
	int temp = a;
	a = b;
	b = temp;
}
void reverseSort(int* digits, int start, int length) {
	int end = length - 1;
	while (start < end) {
		swap(digits[start], digits[end]);
		start++;
		end--;
	}
}

void nextSmallerThan(string input, string output)
{
	//TODO
}

bool nextSmallerThanTests(string input, string output)
{
	ifstream in1(input);
	ifstream in2(output);
	string source1 = "";
	string source2 = "";
	while (getline(in1, source1) && getline(in2, source2))
	{
		int n1 = stoi(source1);
		int n2 = stoi(source2);
		if (n1 != n2)
		{
			in1.close();
			in2.close();
			return false;
		}
	}
	in1.close();
	in2.close();
	return true;
}

void nextSmallerThanTests()
{
	cout << "Test " << (nextSmallerThan(21) == 12 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(531) == 513 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2071) == 2017 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(9) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(111) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(135) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1027) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1113211111) == 1113121111 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(91234567) == 79654321 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(173582) == 173528 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(4321234) == 4314322 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2147483647) == 2147483476 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThanTests("result.txt", "output.txt") ? "Passed." : "Failed.") << endl;
}